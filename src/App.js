
import { Col, Container, Row } from 'react-bootstrap';
import './App.css';
import { useEffect, useState } from 'react';
import SecureFetch from 'secure-fetch';
import 'react-phone-number-input/style.css';
import PhoneInput from 'react-phone-number-input'
function App() {
 const [mobileNumber, setMobileNumber] = useState('');
 const [showOTPfiled, setOTPfiledVisibility] = useState(false);
 const [OTP, setOTP] = useState('');
 const regex = {
  mobileNumber,
  numeric: /^[0-9]*$/,
  alphanumeric: /^\d*[a-zA-Z][a-zA-Z0-9]*$/
 }
 useEffect(() => {
  SecureFetch.initEnvironment('LOCAL');
 })
 const onLoginBtnClick = async() => {
  if (!showOTPfiled) {
   const result = await SecureFetch.run('sendOTP', {
    'Content-Type': 'application/json',
    'AppName': '360App',
    'LanguageCode': 'en',
    'CountryID': 105
   }, {
    "mobileNo": mobileNumber,
    "deviceType": "web",
   });
   console.log('result--', result);
   if (result && result.success) {
    setOTPfiledVisibility(true);
   }
  } else {
   const data = await SecureFetch.run('verifyOTP', {
    'Content-Type': 'application/json',
    'AppName': '360App',
    'LanguageCode': 'en',
    'CountryID': 105
   }, {
    "mobileNo": mobileNumber,
    "otp": OTP,
    "deviceType": "web",
   });
   console.log('data--', data);
  }

 }
 const handle_verify_otp = (v) => {
  console.log("value--",v.target.value);
  setOTP(v.target.value);
 }

 const OnChangeMobileNumber = async (e) => {
  let mNo = e.target.value;
  console.log("mobileNumber--" + mNo);
  if (regex[e.target.name].test(mNo)) {
   console.log("mobileNumber-2-" + mNo);
   setMobileNumber(e.target.value);
  }
 }
 return (
  <Container fluid className='app flex-with-center'>
   <div className='summary-card-parent'>
    <Container>   <Row >
     <Col >
     <span>
      Mobile number
     </span>
     <PhoneInput className="user-details-input"
      placeholder="Enter phone number"
      value={mobileNumber}
      international={false}
      onChange={(e) => {
       console.log(typeof e.target.value)
       OnChangeMobileNumber(e);
      }}/>
   
     </Col> 
    </Row>
     <Row >
      <Col style={{marginTop:'40px'}} className={showOTPfiled ? 'show' : 'hide'}>
       <div >
       <label>Enter OTP: </label>
       <input className='user-otp' type="text" id="fname" name="fname" value ={OTP} onChange={handle_verify_otp}/>
     
       </div>
     
      </Col>
     </Row>
     <Row>
      <Col className='flex-with-center'>
       <div className='trade_in_small_btn' onClick={onLoginBtnClick}>
       {showOTPfiled ? 'Verify OTP' : 'send OTP'}
       </div>
       
      </Col>
     </Row>
    </Container>

   </div>
  </Container>
 );
}
export default App;
